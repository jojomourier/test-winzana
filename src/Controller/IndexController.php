<?php

namespace App\Controller;

use App\Entity\Article;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request)
    {
        //Récupération des articles
        $articles = $this->getDoctrine()
            ->getRepository(Article::class)
            ->findBy([], array('created_at' => 'DESC'), 3, 0);

        //Création du formulaire
        $article = new Article();
        $article->setCreatedAt(new \DateTime());
        $article->setUpdateAt(new \DateTime());
        $article->setImage("moon.jpg");
        $article->setStatut(1);
        $form = $this->createFormBuilder($article)
            ->add('titre', TextType::class)
            ->add('name', TextType::class)
            ->add('name', TextType::class)
            ->add('firstname', TextType::class)
            ->add('city', TextType::class)
            ->add('description', TextareaType::class)
            ->add('save', SubmitType::class, ['label' => 'Créer un article'])
            ->getForm();

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {

            $article = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('index');

        }
        return $this->render('index.html.twig', [
            'articles' => $articles,
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/article/{id}", name="article")
     *
     */
    public function article(int $id){

        $article = $this->getDoctrine()
            ->getRepository(Article::class)
            ->findOneBy(["id" => $id]);


        return new Response(
            '<html><body>Article: '.$article->getTitre().'</body></html>'
        );

    }
}
